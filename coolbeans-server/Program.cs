﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Coolbeans.Server
{
    class Program
    {


		static void Main(string[] args)
        {
			//server srv;


			bool daemonMode = false;
            int port = 0;
            for (int i = 0; i < args.Length; i++) {
                if (args[i].ToLower() == "-d" || args[i].ToLower() == "--daemon")
                    daemonMode = true;
                if (args[i].ToLower() == "port" && i+1 <= args.Length-1 && int.TryParse(args[i + 1], out port) == true)
                    break;
            }

            if (!daemonMode && port == 0) {
                while (port == 0)
                {
                    Console.Write("Enter Listen Port: ");
                    string portstring = Console.ReadLine();
                    if (!int.TryParse(portstring, out port))
                        Console.WriteLine("Couldn't read port number. Has to be and integer between 1 and 65000. Try again.");
                }
            }

			srv = new server(daemonMode);

            Console.WriteLine("Port: {0}", port);
			srv.Port = port;

            Console.ReadKey();
        }

    }
}
