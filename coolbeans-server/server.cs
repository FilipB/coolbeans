﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Coolbeans.Server;

namespace Coolbeans.Server
{
	public delegate void Message_Output(string msg);

	class server : ICoolbeansSystem
	{
		CoolbeansServerLibrary cbserver = new CoolbeansServerLibrary();

		public Message_Output StdOut;
		public Message_Output StdErr;

		private bool daemonMode = false;
		public bool IsDaemon
		{
			get
			{
				return daemonMode;
			}
			set
			{
				daemonMode = value;
			}
		}

		public int Port { set { cbserver.Port = value; } get { return cbserver.Port; } }

		public server(bool daemonMode) : this() { this.IsDaemon = daemonMode; }
		public server(bool daemonMode, int port) : this() { this.Port = port; this.IsDaemon = daemonMode; }

		private server() {
			// set default port
			this.Port = 4444;

			cbserver.Output += new Message_Output(PrintToStdOut);
			cbserver.OutputError += new Message_Output(PrintToStdOut);
		}


		private void PrintToStdOut(string str) {
			Console.WriteLine(str);
		}

		private void WriteToLog(string str)
		{
			//Console.WriteLine(str);
			//TODO : implement this.
			Console.WriteLine("SENDTOLOG: " + str);
		}
	}
}
