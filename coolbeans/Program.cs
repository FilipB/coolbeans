﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coolbeans
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Hello World!\nPress any key to continue...");
            System.Console.ReadKey(false);


			bool daemonMode = false;
			int port = 0;
			for (int i = 0; i < args.Length; i++)
			{
				if (args[i].ToLower() == "-d" || args[i].ToLower() == "--daemon")
					daemonMode = true;
				if (args[i].ToLower() == "port" && i + 1 <= args.Length - 1 && int.TryParse(args[i + 1], out port) == true)
					break;
			}

			if (!daemonMode && port == 0)
			{
				while (port == 0)
				{
					Console.Write("Enter Listen Port: ");
					string portstring = Console.ReadLine();
					if (!int.TryParse(portstring, out port) || !(port >= System.Net.IPEndPoint.MinPort && port <= System.Net.IPEndPoint.MaxPort))
					{
						Console.WriteLine("Couldn't read port number. Has to be and integer between 1 and 65000. Try again.");
						port = 0;
					}
				}
			}

			System.Net.IPEndPoint destEP = null;
			while (destEP == null) {
				Console.Write("Enter Destination IP: ");
				string strIP = Console.ReadLine();
				Console.Write("Enter Destination Port: ");
				string strPort = Console.ReadLine();
				System.Net.IPAddress ip;
				int iPort;
				if (System.Net.IPAddress.TryParse(strIP, out ip) && int.TryParse(strPort, out iPort))
				{
					destEP = new System.Net.IPEndPoint(ip, iPort);
				}
				else {
					Console.WriteLine("Either Destination IP or Port was invalid. Try again.");
				}
			}



			System.Console.WriteLine("Starting to listen on UDP port {0}. Press Escape to quit.",port);
			UDPSocket sock = new UDPSocket(port);
			sock.Open();
			sock.PacketRecieved += sock_MessageRecieved;


			ConsoleKeyInfo cki;
			string userInput = "";
			while ((cki = Console.ReadKey(true)).Key.ToString() != "Escape")
			{
				if (cki.Key.ToString() == "Enter")
				{
					if (userInput != "")
					{
						//System.Net.EndPoint target = new System.Net.IPEndPoint(System.Net.IPAddress.Parse("127.0.0.1"), 1337);
						sock.SendPacket(destEP, Encoding.UTF8.GetBytes(userInput));
						Console.WriteLine();
					}
					userInput = "";
				}
				else
				{
					userInput += cki.KeyChar;
					Console.Write(cki.KeyChar);
				}
			}

			System.Console.WriteLine("Press any key to exit...");
			System.Console.ReadKey(false);
			sock.Close();
        }

		static void sock_MessageRecieved(object sender, EventArgs e)
		{
			Console.WriteLine(((Packet)e).source.ToString() + ": " + Encoding.UTF8.GetString(((Packet)e).data));
		}
    }
}
