﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Sockets;
using System.Net;


namespace coolbeans
{


	class TCPSocket
	{
		private int port = 0;
		public int Port
		{
			set
			{
				if (value >= System.Net.IPEndPoint.MinPort && value <= System.Net.IPEndPoint.MaxPort)
				{
					this.port = value;
					if (this.Status == Status.Open)
					{
						// Socket is open and currently listening. Close and reopen to change to the new port.
						this.Close();
						this.Open();
					}
				}
				else
				{
					throw new ArgumentOutOfRangeException(String.Format("The specified port number ({0}) is out of range of valid ports ({1}-{2}).", value, System.Net.IPEndPoint.MinPort, System.Net.IPEndPoint.MaxPort));
				}
			}
			get { return this.port; }
		}

		public Status Status = Status.Closed;

		private Socket udpSocket;
		private byte[] buffer;

		// NOTE Throws ArgumentOutOfRangeException
		// TODO test
		public TCPSocket(int listenPort) : this() { this.Port = listenPort; }

		public TCPSocket() { }

		public event EventHandler PacketRecieved;

		private void TriggerEvent_PacketRecieved(Socket sock, EndPoint source, byte[] packetData)
		{
			if (this.PacketRecieved != null)
				this.PacketRecieved(this, new Packet(sock, source, packetData));
		}

		public void SendPacket(EndPoint packetDestination, byte[] packetData)
		{
			this.SendPacket(packetDestination, packetData, null, null);
		}

		public void SendPacket(EndPoint packetDestination, byte[] packetData, AsyncCallback callback, object cbState)
		{
			udpSocket.BeginSendTo(packetData, 0, packetData.Length, SocketFlags.None, packetDestination, callback, cbState);
		}

		public void Open()
		{
			//Setup the socket and message buffer
			udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			udpSocket.Bind(new IPEndPoint(IPAddress.Any, this.port));
			buffer = new byte[1024];

			//Start listening for a new message.
			this.Listen(udpSocket);
		}

		private void Listen(Socket socket)
		{
			this.Status = Status.Open;
			EndPoint sourceEP = new IPEndPoint(IPAddress.Any, 0);
			socket.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref sourceEP, ReceivePacket, socket);
		}

		private void StopListen(Socket socket)
		{
			// insert code here for added grace.
			// socket.EndReceiveMessageFrom(
			this.Status = Status.Closed;
		}

		public void Close()
		{
			StopListen(this.udpSocket);
			this.udpSocket.Close();
		}

		private void ReceivePacket(IAsyncResult aResult)
		{
			try
			{
				//Get the received message.
				Socket recvSock = (Socket)aResult.AsyncState;
				EndPoint packetSource = new IPEndPoint(IPAddress.Any, 0);
				int packetSize = 0;
				byte[] packetData = null; // receiveBuffer will be copied here (so the receivebuffer can be reused)
				try
				{
					packetSize = recvSock.EndReceiveFrom(aResult, ref packetSource);
					packetData = new byte[packetSize];
					Array.Copy(buffer, packetData, packetSize);
				}
				catch (SocketException e)
				{
					if (e.ErrorCode == 10054)
					{
						//Port is closed.
						// recieved ICMP packet because destination port is closed. Ignore packet and begin listening for next one.
						this.Listen(recvSock);
						return;
					}
				}

				// Start listening for new packet.
				this.Listen(recvSock);

				// output packet.
				TriggerEvent_PacketRecieved(recvSock, packetSource, packetData);
			}
			catch (ObjectDisposedException)
			{
				//expected termination exception on a closed socket.
				// ...I'm open to suggestions on a better way of doing this.
				if (this.Status != Status.Closed)
				{
					// Allow the closing of the socket without printer error message if Status == closed
					// 
					Console.WriteLine("Error: ObjectDisposedException");
				}
			}
		}

	}
}
