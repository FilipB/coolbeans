﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coolbeans.Server
{

    public class CoolbeansServerLibrary
    {
		ICoolbeansSystem SystemIntegration;

		int port = 0;
		public int Port {
			get {return port;}
			set {
				if (value > 0 && value < 65000) {
					port = value;
					Output(String.Format("Setting port to {0}.", this.port));
				} else {
					//port = 0; //actually, dont set port. Previous port was probably better.
					OutputError(String.Format("Cannot set port to invalid port number ({0}), using port {1}.", value, this.port));
				}
			}
		}

		bool isRunning = false;
		public bool IsRunning { get { return isRunning; } set { if (value == true) this.Start(); else this.Stop(); } }


		public CoolbeansServerLibrary(ICoolbeansSystem sysIntegration, int ListenPort)
			: this(sysIntegration)
		{
			this.Port = ListenPort;
		}

        public CoolbeansServerLibrary(ICoolbeansSystem sysIntegration) :this() {
			this.SystemIntegration = sysIntegration;
        }

		private CoolbeansServerLibrary()
		{

		}

		public void Output(string msg) {
			this.SystemIntegration.StdOut(msg);
		}

		public void OutputError(string msg)
		{
			this.SystemIntegration.StdErr(msg);
		}

		public void Start() {
			isRunning = true;
			// other stuff here too
			Output("Starting server.");
			this.SystemIntegration.OpenPort(this.Port);
		}

		public void Stop()
		{
			isRunning = false;
			// other stuff here too
			Output("Stopping Server.");
		}

    }

	public interface ICoolbeansSystem {
		System.Net.Sockets.Socket OpenSocket(System.Net.Sockets.AddressFamily af, System.Net.Sockets.SocketType st, System.Net.Sockets.ProtocolType pt);
		void ClosePort(System.Net.Sockets.Socket socket);
		System.Net.IPEndPoint GetIPEndPoint(System.Net.IPAddress ip, Int32 port);

		void StdOut(string msg);
		void StdErr(string msg);
	}
}
